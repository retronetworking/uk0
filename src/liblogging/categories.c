
#include <osmocom/core/utils.h>
#include <osmocom/core/logging.h>
#include "categories.h"

/* All logging categories used by this project. */

struct log_info_cat log_categories[] = {
	[DOPTIONS] = {
		.name = "DOPTIONS",
		.description = "config options",
		.color = "\033[0;33m",
	},
	[DPH] = {
		.name = "DPH",
		.description = "PH SAP socket interface",
		.color = "\033[0;33m",
	},
	[DSOUND] = {
		.name = "DSOUND",
		.description = "sound io",
		.color = "\033[0;35m",
	},
	[DDSP] = {
		.name = "DDSP",
		.description = "digital signal processing",
		.color = "\033[0;31m",
       	},
	[DWAVE] = {
		.name = "DWAVE",
		.description = "WAVE file handling",
		.color = "\033[1;33m",
	},
	[DUK0] = {
		.name = "DUK0",
		.description = "U interface processing",
		.color = "\033[1;34m",
	},
};

size_t log_categories_size = ARRAY_SIZE(log_categories);

