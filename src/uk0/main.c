/* UK0 via sound card (main)
 *
 * (C) 2022 by Andreas Eversberg <jolly@eversberg.eu>
 * All Rights Reserved
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <termios.h>
#include <sched.h>
#include <time.h>
#include "../liblogging/logging.h"
#include "../liboptions/options.h"
#include "../libsample/sample.h"
#include "../libsound/sound.h"
#include "uk0.h"

static uk0_t *uk0[2] = { NULL, NULL };
int num_kanal = 1;

static void __attribute__((unused)) *soundif = NULL;
static const char *dsp_device = "";
static int dsp_samplerate = 192000;
static int dsp_buffer = 50;
static const char *uk0_socket[2] = { "uk0", NULL };
static int num_uk0_socket = 0;
static int keyboard_control = 0;
static int measure_speed = 0;
static double speed_correction = 1.0;
static int dsp_interval = 1; /* ms */
static int rt_prio = 1;
static int fast_math = 0;

static void print_usage(const char *app)
{
	printf("Usage: %s [-a hw:0,0] [<options>]\n", app);
}

static void print_help()
{
	/*      -                                                                             - */
	printf(" -h --help\n");
	printf("        This help\n");
	printf(" --config [~/]<path to config file>\n");
	printf("        Give a config file to use. If it starts with '~/', path is at home dir.\n");
	printf("        Each line in config file is one option, '-' or '--' must not be given!\n");
	logging_print_help();
	printf(" -a --audio-device hw:<card>,<device>\n");
	printf("        Sound card and device number (default = '%s')\n", dsp_device);
	printf(" -s --samplerate <rate>\n");
	printf("        Sample rate of sound device (default = '%d')\n", dsp_samplerate);
	printf(" -b --buffer <ms>\n");
	printf("        How many milliseconds are processed in advance (default = '%d')\n", dsp_buffer);
	printf("        A buffer below 10 ms requires low interval like 0.1 ms.\n");
	printf(" -U --uk0-socket <filename>\n");
	printf("        Abstract name of socket. Allows an application to connect to layer 1.\n");
	printf("        (default = '%s')\n", uk0_socket[0]);
	printf("        To provide two interfaces with stereo audio, give two UK0 sockets:\n");
	printf("         -U <left> -U <right>\n");
	printf(" -C --control\n");
	printf("        Use keyboard to control activation/deactivation/loopback for testing.\n");
	printf("        This only applies to the first interface, if two interfaces are used.\n");
	printf(" -M --measure-speed\n");
	printf("        Measures clock speed of sound card. Let it run for 10 minutes and use\n");
	printf("        displayed speed factor for correction.\n");
	printf(" -S --speed-correction <value>\n");
	printf("        A sound card, especially USB sound adapters have poor clock souce.\n");
	printf("        Give measured speed factor of sound card to correct clock.\n");
	printf("        (default = %.6f)\n", speed_correction);
	printf(" -r --realtime <prio>\n");
	printf("        Set prio: 0 to disable, 99 for maximum (default = %d)\n", rt_prio);
	printf("    --fast-math\n");
	printf("        Use fast math approximation for slow CPU / ARM based systems.\n");
	printf("\n");
	printf("Press 'w' key to toggle display of RX wave form.\n");
	printf("Press 'm' key to toggle display of measurement values.\n");
}

#define OPT_FAST_MATH           1001

static void add_options(void)
{
	option_add('h', "help", 0);
	option_add('v', "verbose", 1);
	option_add('a', "audio-device", 1);
	option_add('s', "samplerate", 1);
	option_add('b', "buffer", 1);
	option_add('U', "uk0-socket", 1);
	option_add('C', "control", 0);
	option_add('M', "measure-speed", 0);
	option_add('S', "speed-correction", 1);
	option_add('r', "realtime", 1);
	option_add(OPT_FAST_MATH, "fast-math", 0);
}

static int handle_options(int short_option, int argi, char **argv)
{
	int rc;

	switch (short_option) {
	case 'h':
		print_usage(argv[0]);
		print_help();
		return 0;
	case 'v':
		rc = parse_logging_opt(argv[argi]);
		if (rc > 0)
			return 0;
		if (rc < 0) {
			fprintf(stderr, "Failed to parse debug option, please use -h for help.\n");
			return rc;
		}
		break;
	case 'a':
		dsp_device = options_strdup(argv[argi]);
		break;
	case 's':
		dsp_samplerate = atoi(argv[argi]);
		break;
	case 'b':
		dsp_buffer = atoi(argv[argi]);
		break;
	case 'U':
		if (num_uk0_socket == 2) {
			fprintf(stderr, "Cannot provide more than two interfaces. Stereo audio allows just two interfaces.\n");
			return -EINVAL;
		}
		uk0_socket[num_uk0_socket++] = options_strdup(argv[argi]);
		num_kanal = num_uk0_socket;
		break;
	case 'C':
		keyboard_control = 1;
		break;
	case 'M':
		measure_speed = 1;
		break;
	case 'S':
		speed_correction = atof(argv[argi]);
		break;
	case 'r':
		rt_prio = atoi(argv[argi]);
		break;
	case OPT_FAST_MATH:
		fast_math = 1;
		break;
	default:
		return -EINVAL;
	}
	return 1;
}

static int quit = 0;
static void sighandler(int sigset)
{
	if (sigset == SIGHUP || sigset == SIGPIPE)
		return;

	fprintf(stderr, "\nSignal %d received.\n", sigset);

	quit = 1;
}

static int get_char()
{
        struct timeval tv = {0, 0};
        fd_set fds;
        char c = 0;
        int __attribute__((__unused__)) rc;

        FD_ZERO(&fds);
        FD_SET(0, &fds);
        select(0+1, &fds, NULL, NULL, &tv);
        if (FD_ISSET(0, &fds)) {
                rc = read(0, &c, 1);
                return c;
        } else
                return -1;
}

static void help_keyboard(void)
{
	printf("Use keyboard to control interface:\n");
	printf(" 'e' = enable (unblock) interface (do this first!)\n");
	printf(" 'd' = disable (block) interface\n");
	printf(" '1' = activate interface (LT side activation)\n");
	printf(" '0' = deactivate interface\n");
	printf(" 'l' = toggle loopback 2 mode\n");
	printf(" 't' = toggle test signal\n");
}

static void process_keyboard(uk0_t *uk0, char c)
{
	switch (c) {
	case 'e':
		LOGP(DUK0, LOGL_NOTICE, "Keyboard enables interface.\n");
		if (uk0->state != UK0_STATE_NULL) {
			LOGP(DUK0, LOGL_NOTICE, "Interface is already enabled!\n");
			break;
		}
		uk0_handle_event(uk0, UK0_EVENT_ENABLE);
		break;
	case 'd':
		LOGP(DUK0, LOGL_NOTICE, "Keyboard disables interface.\n");
		if (uk0->state == UK0_STATE_NULL) {
			LOGP(DUK0, LOGL_NOTICE, "Interface is already disabled!\n");
			break;
		}
		uk0_handle_event(uk0, UK0_EVENT_DISABLE);
		break;
	case '1':
		LOGP(DUK0, LOGL_NOTICE, "Keyboard activates interface.\n");
		if (uk0->state == UK0_STATE_NULL) {
			LOGP(DUK0, LOGL_NOTICE, "Cannot activate interface, enable first!\n");
			break;
		}
		uk0_handle_event(uk0, UK0_EVENT_ACTIVATE);
		break;
	case '0':
		LOGP(DUK0, LOGL_NOTICE, "Keyboard deactivates interface.\n");
		if (uk0->state == UK0_STATE_NULL) {
			LOGP(DUK0, LOGL_NOTICE, "Cannot deactivate interface, it is disabled!\n");
			break;
		}
		uk0_handle_event(uk0, UK0_EVENT_DEACTIVATE);
		break;
	case 'l':
		if (!uk0->loopback2) {
			LOGP(DUK0, LOGL_NOTICE, "Keyboard activates loopback 2.\n");
			uk0_handle_event(uk0, UK0_EVENT_LOOP2_ENABLE);
		} else {
			LOGP(DUK0, LOGL_NOTICE, "Keyboard deactivates loopback 2.\n");
			uk0_handle_event(uk0, UK0_EVENT_LOOP_DISABLE);
		}

		break;
	case 't':
		LOGP(DUK0, LOGL_NOTICE, "Keyboard toggels test singal.\n");
		uk0_handle_event(uk0, UK0_EVENT_TESTSIGNAL);
		break;
	}
}

static int soundif_open(const char *audiodev, int __attribute__((unused)) samplerate, int __attribute__((unused)) buffer_size)
{
	if (!audiodev || !audiodev[0]) {
		LOGP(DDSP, LOGL_ERROR, "No audio device given!\n");
		return -EINVAL;
	}

	/* open audiodev */
#ifdef HAVE_ALSA
	soundif = sound_open(SOUND_DIR_DUPLEX, audiodev, NULL, NULL, NULL, num_kanal, 0.0, samplerate, buffer_size, 1.0, 1.0, 0.0, 2.0);
	if (!soundif) {
		LOGP(DDSP, LOGL_ERROR, "Failed to open sound device!\n");
		return -EIO;
	}
#else
	LOGP(DDSP, LOGL_ERROR, "No audio device is compiled in! Cannot use UK0 interface.\n");
	return -EINVAL;
#endif

	return 0;
}

static void soundif_start(void)
{
#ifdef HAVE_ALSA
	sound_start(soundif);
	LOGP(DDSP, LOGL_DEBUG, "Starting audio stream!\n");
#endif
}

static void soundif_close(void)
{
#ifdef HAVE_ALSA
	/* close audiodev */
	if (soundif) {
		sound_close(soundif);
		soundif = NULL;
	}
#endif
}

static double get_time(void)
{
	static struct timespec tv;

	clock_gettime(CLOCK_REALTIME, &tv);

	return (double)tv.tv_sec + (double)tv.tv_nsec / 1000000000.0;
}

#ifdef HAVE_ALSA
static double speed_start = 0.0;
static uint64_t speed_count, speed_interval;
#endif

static void soundif_work(int __attribute__((unused)) buffer_size)
{
#ifdef HAVE_ALSA
	int count, i;
	sample_t buff[num_kanal][buffer_size], *samples[num_kanal];
	double rf_level_db[num_kanal];
	for (i = 0; i < num_kanal; i++)
		samples[i] = buff[i];
	int rc;
	double diff;

	/* encode and write */
	count = sound_get_tosend(soundif, buffer_size);
	if (count < 0) {
		LOGP(DDSP, LOGL_ERROR, "Failed to get number of samples in buffer (rc = %d)!\n", count);
		return;
	}
	if (count) {
		for (i = 0; i < num_kanal; i++)
			uk0_encode_mms43(uk0[i], samples[i], count);
		rc = sound_write(soundif, samples, NULL, count, NULL, NULL, num_kanal);
		if (rc < 0) {
			LOGP(DDSP, LOGL_ERROR, "Failed to write TX data to audio device (rc = %d)\n", rc);
			return;
		}
	}

	/* read */
	count = sound_read(soundif, samples, buffer_size, num_kanal, rf_level_db);
	if (count < 0) {
		LOGP(DDSP, LOGL_ERROR, "Failed to read from audio device (rc = %d)!\n", count);
		return;
	}

	/* measure speed */
	if (measure_speed) {
		if (!speed_start) {
			speed_start = get_time();
			speed_count = 0;
			speed_interval = 0;
		} else {
			speed_count += count;
			speed_interval += count;
		}
		if (speed_interval / dsp_samplerate >= 5) {
			diff = get_time() - speed_start;
			LOGP(DDSP, LOGL_NOTICE, "Speed factor measurement: %.6f\n", (double)speed_count / (double)dsp_samplerate / diff);
			speed_interval = 0;
		}
	}

	/* decode */
	for (i = 0; i < num_kanal; i++)
		uk0_decode_mms43(uk0[i], samples[i], count);
#endif
}

int main(int argc, char *argv[])
{
	int argi, rc;
	int buffer_size;
	struct termios term, term_orig;
	double begin_time, now, sleep;
	int i, j;
	char c;

	logging_init();

        /* handle options / config file */
	add_options();
	rc = options_config_file(argc, argv, "~/.osmocom/uk0/uk0.conf", handle_options);
	if (rc < 0)
		return 0;
	argi = options_command_line(argc, argv, handle_options);
	if (argi <= 0)
		return argi;

	if (dsp_samplerate < 192000) {
		fprintf(stderr, "Error: Given sample rate must be at least 192000, to make UK0 work!\n");
		return -EINVAL;
	}

	uk0_init(fast_math);

	/* size of dsp buffer in samples */
	buffer_size = dsp_samplerate * dsp_buffer / 1000;

	rc = soundif_open(dsp_device, dsp_samplerate, buffer_size);
	if (rc < 0) {
		printf("Failed to open UK0 device, use '-h' for help.\n");
		goto error;
	}

	/* create and init UK0 instances */
	for (i = 0; i < num_kanal; i++) {
		uk0[i] = uk0_create(uk0_socket[i], dsp_samplerate, speed_correction, buffer_size);
		if (!uk0[i])
			goto error;
		for (j = 0; j < 3; j++) {
			hdlc_tx_init(&uk0[i]->hdlc_tx[j], uk0[i], j + 1, HDLC_MODE_OFF);
			hdlc_rx_init(&uk0[i]->hdlc_rx[j], uk0[i], j + 1, HDLC_MODE_OFF);
		}
		ph_socket_init(&uk0[i]->ph_socket, ph_socket_rx_msg, uk0[i], uk0_socket[i], 1);
	}

	if (keyboard_control)
		help_keyboard();

	/* set real time prio */
	if (rt_prio) {
		struct sched_param schedp;

		memset(&schedp, 0, sizeof(schedp));
		schedp.sched_priority = rt_prio;
		rc = sched_setscheduler(0, SCHED_RR, &schedp);
		if (rc)
			fprintf(stderr, "Error setting SCHED_RR with prio %d\n", rt_prio);
	}

	/* prepare terminal */
	tcgetattr(0, &term_orig);
	term = term_orig;
	term.c_lflag &= ~(ISIG|ICANON|ECHO);
	term.c_cc[VMIN]=1;
	term.c_cc[VTIME]=2;
	tcsetattr(0, TCSANOW, &term);

	signal(SIGINT, sighandler);
	signal(SIGHUP, sighandler);
	signal(SIGTERM, sighandler);
	signal(SIGPIPE, sighandler);

	soundif_start();

	while (!quit) {
		begin_time = get_time();

		soundif_work(buffer_size);

		osmo_select_main(1);

		c = get_char();
		switch (c) {
		case 3:
			printf("CTRL+c received, quitting!\n");
			quit = 1;
			break;
		case 'w':
			/* toggle wave display */
			display_measurements_on(0);
                        display_wave_on(-1);
			break;
		case 'm':
			/* toggle measurements display */
			display_wave_on(0);
			display_measurements_on(-1);
			break;
		default:
			if (keyboard_control)
				process_keyboard(uk0[0], c);
		}

		display_measurements(dsp_interval / 1000.0);

		now = get_time();

		/* sleep interval */
		sleep = ((double)dsp_interval / 1000.0) - (now - begin_time);
		if (sleep > 0)
			usleep(sleep * 1000000.0);
	}

	signal(SIGINT, SIG_DFL);
	signal(SIGTSTP, SIG_DFL);
	signal(SIGHUP, SIG_DFL);
	signal(SIGTERM, SIG_DFL);
	signal(SIGPIPE, SIG_DFL);

	/* reset real time prio */
	if (rt_prio > 0) {
		struct sched_param schedp;

		memset(&schedp, 0, sizeof(schedp));
		schedp.sched_priority = 0;
		sched_setscheduler(0, SCHED_OTHER, &schedp);
	}

	/* reset terminal */
	tcsetattr(0, TCSANOW, &term_orig);

error:
	/* destroy UK0 instances */
	for (i = 0; i < num_kanal; i++) {
		if (uk0[i]) {
			ph_socket_exit(&uk0[i]->ph_socket);
			uk0_destroy(uk0[i]);
		}
	}

	soundif_close();

	uk0_exit();

	options_free();

	return 0;
}

