
enum hdlc_mode {
	HDLC_MODE_OFF = 0,
	HDLC_MODE_TRANS,
	HDLC_MODE_HDLC,
};

enum hdlc_state {
	HDLC_STATE_IDLE = 0,	/* wait for flag / no data */
	HDLC_STATE_RX_FRAME,	/* receive frame */
	HDLC_STATE_TX_START_FLAG,/* transmit flag */
	HDLC_STATE_TX_DATA,	/* transmit data */
	HDLC_STATE_TX_CRC,	/* transmit crc */
	HDLC_STATE_TX_END_FLAG,	/* transmit flag */
};

struct uk0;

typedef struct hdlc_tx {
	struct uk0 *uk0;
	int channel;
	enum hdlc_mode mode;
	enum hdlc_state state;	/* current frame state */
	uint8_t frame;		/* shit register to detect framing and stuffing */
	uint8_t buffer[65536];
	uint16_t crc;
	int length, index, bits;
} hdlc_tx_t;

typedef struct hdlc_rx {
	struct uk0 *uk0;
	int channel;
	enum hdlc_mode mode;
	enum hdlc_state state;	/* current frame state */
	uint8_t frame;		/* shit register to detect framing and stuffing */
	uint8_t buffer[65537];	/* one more, because it contains end flag */
	int index, bits;
} hdlc_rx_t;

void hdlc_tx_init(hdlc_tx_t *tx, struct uk0 *uk0, int channel, enum hdlc_mode mode);
void hdlc_rx_init(hdlc_rx_t *rx, struct uk0 *uk0, int channel, enum hdlc_mode mode);

